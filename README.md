Minetest Game mod: wool
=======================
See license.txt for license information.

Authors of source code
----------------------
Originally by Perttu Ahola (celeron55) <celeron55@gmail.com> (MIT)
Various Minetest developers and contributors (MIT)

Authors of media (textures)
---------------------------
PixelBox wool textures by Gambit (WTFPL):
  wool_black.png wool_brown.png wool_dark_green.png wool_green.png
  wool_magenta.png wool_pink.png wool_violet.png wool_yellow.png
  wool_blue.png wool_cyan.png wool_dark_grey.png wool_grey.png
  wool_orange.png wool_red.png wool_white.png

Changes by TenPlus1
-------------------
Coloured wool can be dyed back to white and wool blocks now have their
own wool sounds.  Wool blocks can also be used as fuel in furnace.

Sound is from freesfx.co.uk and has a Creative Commons License
