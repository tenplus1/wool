-- wool/init.lua

-- Translation support
local S = minetest.get_translator("wool")

-- new sound for wool
function default.node_wool_defaults(tbl)

	tbl = tbl or {}

	tbl.footstep = tbl.footstep or {name = "wool_coat_movement", gain = 1.0}
	tbl.dug = tbl.dug or {name = "wool_coat_movement", gain = 0.25}
	tbl.place = tbl.place or {name = "default_place_node", gain = 1.0}

	return tbl
end

local dyes = dye.dyes

for i = 1, #dyes do

	local name, desc = unpack(dyes[i])
	local color_group = "color_" .. name

	minetest.register_node("wool:" .. name, {
		description = S(desc .. " Wool"),
		tiles = {"wool_" .. name .. ".png"},
		is_ground_content = false,
		groups = {
			snappy = 2, choppy = 2, oddly_breakable_by_hand = 3,
			flammable = 3, wool = 1, [color_group] = 1
		},
		sounds = default.node_wool_defaults()
	})

	minetest.register_craft{
		output = "wool:" .. name,
		recipe = {
			{"group:wool", "group:dye," .. color_group}
		}
	}
end

-- Legacy (Backwards compatibility with jordach's 16-color wool mod)
minetest.register_alias("wool:dark_blue", "wool:blue")
minetest.register_alias("wool:gold", "wool:yellow")

-- use wool as fuel
minetest.register_craft({
	type = "fuel",
	recipe = "group:wool",
	burntime = 2
})

-- Dummy calls to S() to allow translation scripts to detect the strings.
-- To update this run:
-- for _,e in ipairs(dye.dyes) do print(("S(%q)"):format(e[2].." Wool")) end

--[[
S("White Wool")
S("Grey Wool")
S("Dark Grey Wool")
S("Black Wool")
S("Violet Wool")
S("Blue Wool")
S("Cyan Wool")
S("Dark Green Wool")
S("Green Wool")
S("Yellow Wool")
S("Brown Wool")
S("Orange Wool")
S("Red Wool")
S("Magenta Wool")
S("Pink Wool")
--]]


print ("[MOD] Wool loaded")
